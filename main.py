from quart import Quart, render_template, request
from utils import process_data, get_cards, get_data
import json

app = Quart(__name__)
data = get_data()


@app.route("/")
async def index():
    global data
    args = request.args
    print(args.get("r"))
    data = process_data(data, receiver=args.get("r", "There"))
    return await render_template(f"index.html", data=data)


@app.route('/cake')
async def cake():
    return await render_template(f"3d-cake/index.html")


@app.route('/cards')
async def cards():
    folder = get_cards()
    return await render_template(f"{folder}/index.html", data=data)


if __name__ == "__main__":
    app.run("0.0.0.0", port=443)

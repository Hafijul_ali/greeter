const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = THREE.PCFSoftShadowMap; // default THREE.PCFShadowMap
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

const scene = new THREE.Scene();
scene.background = new THREE.Color("rgb( 193, 237, 233 )");

var amblight = new THREE.AmbientLight(0xffffff, 0.1); //Add ambient light
//scene.add(amblight);

var directlight2 = new THREE.DirectionalLight(0xffffff, 0.5); //Add directional light
directlight2.castShadow = true; //Directional light to cast shadows
scene.add(directlight2);
directlight2.position.set(2, 10, 10);

var pointLight1 = new THREE.PointLight(0xffffff, 4, 1, 2);
pointLight1.position.y = 1.5;
scene.add(pointLight1);

const camera = new THREE.PerspectiveCamera(
  60,
  window.innerWidth / window.innerHeight,
  0.1,
  1000);

camera.position.x = 0; //Position camera on X axis
camera.position.y = 3; //Position camera on Y axis
camera.position.z = 10; //Position camera on Z axis

const controls = new THREE.OrbitControls(camera, renderer.domElement);
controls.maxPolarAngle = Math.PI * 0.4;
controls.enableZoom = true;

const candleFire = new THREE.PointLight(0xffffff, 0.8, 25);
candleFire.castShadow = true;
candleFire.shadow.camera.far = 600;
candleFire.position.y = 5.6;
candleFire.position.z = 0;
scene.add(candleFire);

var visibleFlameGeom = new THREE.CylinderGeometry(0.01, 0.15, 0.5, 4, 4);
var flameModifier = new THREE.SubdivisionModifier(4);
var flameSmooth = flameModifier.modify(visibleFlameGeom);
var visibleFlameMat = new THREE.MeshPhongMaterial({
  color: 0xffffff,
  emissive: 0xff7326,
  specular: 0xffffff,
  shininess: 0.5
});

var visibleFlame = new THREE.Mesh(flameSmooth, visibleFlameMat);
visibleFlame.position.y = candleFire.position.y - 1.1;
scene.add(visibleFlame);

var universeGeom = new THREE.SphereGeometry(60, 32, 32);
var universeMat = new THREE.MeshPhongMaterial({
  color: '0x000000',
  side: THREE.BackSide
});

var universe = new THREE.Mesh(universeGeom, universeMat);
// scene.add(universe);

var points = [];
for (var i = 0; i < 10; i++) {
  points.push(new THREE.Vector2(Math.sin(i * 0.2) * 10 + 5, (i - 5) * 2));
}

var cakeGeom = new THREE.CylinderGeometry(5, 5, 1, 6, 6);
var modifier = new THREE.SubdivisionModifier(4);
var smooth = modifier.modify(cakeGeom);
var cakePink = new THREE.MeshPhongMaterial({
  color: 0xffe9fc,
  emissive: 0xc9669c,
  emissiveIntensity: 0.3,
  shininess: 0
});

var cakeBeige = new THREE.MeshPhongMaterial({
  color: 0xf5d598,
  emissive: 0xfff2b0,
  emissiveIntensity: 0.3,
  shininess: 0
});

var cake1 = new THREE.Mesh(smooth, cakePink);
var cake2 = new THREE.Mesh(smooth, cakeBeige);
var cake3 = new THREE.Mesh(smooth, cakePink);
var cake4 = new THREE.Mesh(smooth, cakeBeige);
cake1.receiveShadow = true;
cake1.castShadow = true;
cake1.position.y = 0;
cake2.receiveShadow = true;
cake2.castShadow = true;
cake2.position.y = 0.8;
cake3.receiveShadow = true;
cake3.castShadow = true;
cake3.position.y = 1.6;
cake4.receiveShadow = true;
cake4.castShadow = true;
cake4.position.y = 2.4;

scene.add(cake1);
scene.add(cake2);
scene.add(cake3);
scene.add(cake4);

var bougieGeom = new THREE.CylinderGeometry(0.1, 0.1, 2, 25, 6);
var bougieMat = new THREE.MeshStandardMaterial({ color: 0xffffff, emissive: 0xf2f2f2, emissiveIntensity: 0.3 });
var bougie = new THREE.Mesh(bougieGeom, bougieMat);
bougie.position.y = 3.2;
bougie.castShadow = true;
scene.add(bougie);

/* var planeGeom = new THREE.PlaneGeometry(1000, 1000, 4, 4);
var planeMat = new THREE.MeshPhongMaterial({ color: 0xC1EDE9, shininess: 100 });
var plane = new THREE.Mesh(planeGeom, planeMat);
plane.rotation.x = -Math.PI / 2;
plane.position.y = -0.5;
plane.receiveShadow = true; */

var planeGeo = new THREE.PlaneGeometry(200, 200, 1, 1); //Add plane/floor mesh
var planeMat = new THREE.MeshLambertMaterial({ color: 0xC1EDE9, transparent: true, opacity: 0.1 }); //Add Lambert material
var plane = new THREE.Mesh(planeGeo, planeMat); //Create mesh object

plane.receiveShadow = true; //Receive shadows
plane.rotation.x = -Math.PI / 2; //Rotate plane 90 degrees

scene.add(plane);


camera.position.z = 20;
camera.position.y = 10;
camera.lookAt(cake1.position);

const textLoaderHB = new THREE.FontLoader();
textLoaderHB.crossOrigin = ''; //Prevent errors with diferent servers sources

const textFontHB = textLoaderHB.parse(font);

const textGeoHB = new THREE.TextGeometry('Happy Birthday', {
  font: textFontHB,
  size: 1,
  height: 0.25,
  curveSegments: 12,
  bevelThickness: 0.03,
  bevelSize: 0.01,
  bevelEnabled: true
});


var textMatHB = new THREE.MeshToonMaterial({ color: 0x2194ce });
var textHB = new THREE.Mesh(textGeoHB, textMatHB); //Create mesh
textHB.castShadow = true; //Cast shadows

textHB.position.x = -5; //Move cake group layer in X axis
textHB.position.y = 0.25; //Move cake group layer in Y axis
textHB.position.z = 5; //Move cake group layer in Z axis

textHB.rotation.x = -Math.PI / 10;

scene.add(textHB);

var frameX = 0.5;
var frameY = 0.5;
var frameZ = 0.5;

var animate = function () {
  //light animation

  requestAnimationFrame(animate);

  candleFire.intensity += Math.sin(frameX) / 30 / 3;
  candleFire.position.x += Math.sin(frameX) / 80 / 3;
  candleFire.position.z += Math.sin(frameZ) / 80 / 3;
  candleFire.position.y += Math.sin(frameY) / 80 / 3;

  visibleFlame.scale.y += Math.sin(frameY) / 20;
  visibleFlame.scale.x += Math.sin(frameX) / 30;
  visibleFlame.scale.z += Math.sin(frameZ) / 10;

  frameY += 0.3;
  frameX += 0.4;
  frameZ += 0.2;
  //cube.rotation.x += 0.1;
  //cube.rotation.y += 0.1;

  renderer.render(scene, camera);
};

animate();

// window.addEventListener("resize", onWindowResize, false);

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(window.innerWidth, window.innerHeight);
}
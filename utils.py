from datetime import date
from calendar import month_abbr
from random import randint
import json


def get_data():
    with open('data.json') as data:
        return json.load(data)


def process_data(data, **args):
    data['receiver'] = args.get('receiver', "There")
    year, month, day = str(date.today()).split("-")
    data['event']['date'] = day
    data['event']['month'] = month_abbr[int(month)]
    data['event']['year'] = year
    with open("data.json", "w") as file:
        json.dump(data, file)
    return data


def get_cards():
    cards = ['3d-card', '3d-flip-card', 'fade-out-card', '3d-transform-card']
    return cards[randint(0, len(cards)-1)]
